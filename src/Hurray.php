<?php

namespace Hurray;

class Hurray {

    protected $elements;
    protected $conditions;
    protected $fetchables;

    public function __construct(array $elements)
    {
        $this->elements = $elements;
    }

    public function where(string $column, string $compare, $value, string $condition = 'AND')
    {
        if (isset($this->conditions[0]) === false) {
            $this->conditions[] = new ConditionGroup(new Condition($column, $compare, $value));
        } else {
            $this->conditions[0]->injectCondition(new Condition($column, $compare, $value));
        }
        return $this;
    }

    public function orWhere(string $column, string $compare, $value)
    {
        if (isset($this->conditions[1]) === false) {
            $this->conditions[] = new ConditionGroup(new Condition($column, $compare, $value), ConditionGroup::OR_COND);
        } else {
            $this->conditions[1]->injectCondition(new Condition($column, $compare, $value));
        }

        return $this;
    }

    public function get(array $columns)
    {
        if (empty($this->elements) || empty($columns) || empty($this->conditions)) {
            return [];
        }

        $elements = [];

        foreach ($this->elements as $element) {
            if ($this->isPassed($element)) {
                $elements[] = $element;
            }
        }

        return $elements;
    }

    protected function isPassed($element)
    {
        $return = false;

        foreach ($this->conditions as $condition) {
            $return |= $condition->checkCondition($element);
        }

        return (bool) $return;
    }

}

class ConditionGroup {

    /**
     * This Group class will only host
     * conditions with same type
     * eg. AND || OR
     */
    const AND_COND = 1;
    const OR_COND = 2;

    protected $type;
    protected $conditions;

    public function __construct($conditions, $type = self::AND_COND)
    {
        if (is_array($conditions)) {
            $this->conditions = $conditions;
        } else {
            $this->conditions = [$conditions];
        }

        $this->type = $type;
    }

    public function injectCondition(Condition $condition)
    {
        $this->conditions[] = $condition;
    }

    public function checkCondition($element)
    {
        if (empty($this->conditions)) {
            return true;
        }

        $return = true;

        foreach ($this->conditions as $condition) {
            $conditionReturn = $condition->checkCondition($element);

            if ($this->type === self::OR_COND) {
                if ($conditionReturn) {
                    return true;
                }
                $return = false;
            } elseif ($this->type === self::AND_COND) {
                $return &= $conditionReturn;
            }
        }

        return (bool) $return;
    }

    public function getType()
    {
        return $this->type;
    }

}

class Condition {

    const EQUAL = '=';
    const LESS_THAN = '<';
    const GREATER_THAN = '>';

    protected $column;
    protected $value;
    protected $compare;

    public function __construct($column, $compare, $value)
    {
        $this->column = $column;
        $this->compare = $compare;
        $this->value = $value;
    }

    public function checkCondition($element)
    {
        if (isset($element[$this->column]) === false) {
            return false;
        }

        if ($this->compare === self::EQUAL) {
            return $element[$this->column] === $this->value;
        } else if ($this->compare === self::LESS_THAN) {
            return $element[$this->column] < $this->value;
        } else if ($this->compare === self::GREATER_THAN) {
            return $element[$this->column] > $this->value;
        }

        return false;
    }

}
